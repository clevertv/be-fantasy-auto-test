package com.espn.test.perf.dataLoader

import java.nio.charset.StandardCharsets
import java.nio.file.{Files, Paths}

object DataLoaderCreatePlayers {

  def main(args: Array[String]): Unit = {

//    val gameXML: String = createPlayersUpdate(
//     nrOfPlayers = 1625
//    )
//    saveToFile("player", gameXML)
//
 }

  private def saveToFile(fileName: String, data: String ): Unit = {
    Files.write(Paths.get(s"""./espn-tests-perf/src/main/resources/feed/$fileName.xml"""), data.getBytes(StandardCharsets.UTF_8))
  }

}
