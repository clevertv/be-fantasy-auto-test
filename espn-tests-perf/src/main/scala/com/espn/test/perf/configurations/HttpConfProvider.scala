package com.espn.test.perf.configurations

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.http.protocol.HttpProtocolBuilder
import com.espn.test.model.general._

object HttpConfProvider extends Config {

  trait WebCustomerUrl {
    val httpConf = Utils.buildHttpConfiguration(env._env)
  }

}

private object Utils {
  def buildHttpConfiguration(
                              url: String,
                              contentType: String = "application/json",
                              mediaTypes: String = "application/json"): HttpProtocolBuilder = {
    http
      .baseUrl(url)
      .contentTypeHeader(contentType)
      .acceptHeader(mediaTypes)
      .header("X-Client-Req-Id", "asdadasdadsa")
  }
}
