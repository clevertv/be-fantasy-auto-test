package com.espn.test.perf.dataLoader

import java.util.concurrent.ConcurrentLinkedQueue

import com.espn.test.model.domain.accounts.{CreateAccounts, ReceivedUserAccount}
import com.espn.test.model.general.Config
import com.espn.test.model.requests.accounts.UserSignUp
import com.espn.test.model.samplers.CrearteAccount
import com.espn.test.model.utils.{JsonWrapper, WriteInFile}
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import io.gatling.core.feeder.Feeder
import org.json4s.DefaultFormats
import org.json4s.jackson.Serialization.write

import scala.collection.mutable.ListBuffer
import scala.collection.parallel.ParSeq

object DataNewCustomer extends App with WriteInFile with Config {

    val userAccounts = new ConcurrentLinkedQueue[CreateAccounts]()

    val mapper = new ObjectMapper()
    mapper.registerModule(DefaultScalaModule)
    mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL)

    ParSeq(1 to dataLoader.numberOfCreateUsers: _*).foreach(u => {
      val (_, body: CreateAccounts, mail, pass) = createCustomer()
      val userID: ReceivedUserAccount = UserSignUp.getCreateUserNotEspn(mail, pass)
      userAccounts.add(body.copy(userId = Some(userID.ctv_user_id)))
      println(body)
    })

    val usersCreated = new ListBuffer[CreteListOfUsersJson]
    userAccounts.toArray.foreach((u: AnyRef)=>{
      usersCreated += CreteListOfUsersJson(
        mapper.writeValueAsString(u),
        u.asInstanceOf[CreateAccounts].profile.email,
        u.asInstanceOf[CreateAccounts].password,
        u.asInstanceOf[CreateAccounts].userId.get
      )
    })

    val json = new JsonWrapper(usersCreated.toArray)asPrettyJsonString

    saveToFile("user", json, "json")

  case class CreteListOfUsersJson(
                                   bodyUsers: String,
                                   email: String,
                                   password: String,
                                   userId: Int
                                 )

  def createCustomer() = {

    val newCustomer = CrearteAccount.createCustomer()

    implicit val formats: DefaultFormats.type = DefaultFormats
    val userInJson = write(newCustomer)

    (userInJson, newCustomer, newCustomer.profile.email, newCustomer.password)
  }

  def feederCreateCustomer: Feeder[String] = new Feeder[String] {
    override def hasNext: Boolean = true

    override def next(): Map[String, String] = {

      val (bodyNewCustomer, _, _, _ ) = createCustomer()

      Map(
        "bodyNewCustomer" -> bodyNewCustomer
      )
    }
  }
}
