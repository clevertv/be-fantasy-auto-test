package com.espn.test.perf.dataLoader

import java.nio.charset.StandardCharsets
import java.nio.file.{Files, Paths}



object DataLoaderCreateGame {

  def main(args: Array[String]): Unit = {


//    val gameXML: String = createGame(
//      nrOfCategory = 1,
//      nrOfPlayer = 4,
//      nrOfRule = 5,
//      nrOfSide = 2,
//      nrOfStatisticsPeriod = 2
//    )
//    saveToFile("game", gameXML)
  }

  private def saveToFile(fileName: String, data: String ): Unit = {
    Files.write(Paths.get(s"""./espn-tests-perf/src/main/resources/feed/$fileName.xml"""), data.getBytes(StandardCharsets.UTF_8))
  }

}
