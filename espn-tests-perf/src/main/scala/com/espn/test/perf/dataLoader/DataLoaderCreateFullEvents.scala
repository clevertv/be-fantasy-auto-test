package com.espn.test.perf.dataLoader

import com.espn.test.model.domain.uploads.{Game, SelectCom}
import com.espn.test.model.general.Config
import com.espn.test.model.requests.View.ViewCompetitionsRequests
import com.espn.test.model.samplers.uploades.CompetitionsSample._
import com.espn.test.model.samplers.uploades.FixturesSampler._
import com.espn.test.model.samplers.uploades.GamesSample._
import com.espn.test.model.samplers.uploades.PlayersSample._
import com.espn.test.model.utils.reader.ReadFromXML.reader

import scala.util.matching.Regex

object DataLoaderCreateFullEvents extends App with Config {

  val (gameName, gameXml) = createGame(
    nrOfSide = dataLoader.numberOfSideForGame,
    nrOfCategory = dataLoader.numberOfCategoryForGame,
    nrOfPlayer = dataLoader.numberOfPlayerForGame,
    nrOfStatisticsPeriod = dataLoader.numberOfStatisticsPeriodForGame,
    nrOfRule = dataLoader.numberOfRuleForGame
  )

  val gameData: Array[SelectCom] = reader("Game")
  val idPattern = new Regex("""\d+(?:">gameName)""")

  val competition = crearteCompetition(
    nrOfLegend = dataLoader.numberOfLegendForCompetition,
    nrOfActivationDate = dataLoader.numberOfActivationDateForCompetition,
    nrOfPartialPrize = dataLoader.numberOfPartialPrizeForCompetition,
    nrOfTransferPeriod = dataLoader.numberOfTransferPeriodForCompetition,
    gameData = gameData(0).asInstanceOf[Game]
  )

  val idGames = idPattern.findFirstIn(ViewCompetitionsRequests.getIdGames().toString).get.split("[a-zA-Z\"=> ]").mkString("")



  val fixture = createFixtures(
    gameData = gameData(0).asInstanceOf[Game],
    idGames
  )

  val players = createPlayersUpdate(
    gameData = gameData(0).asInstanceOf[Game]
  )

}
