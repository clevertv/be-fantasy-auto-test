package requests

import com.espn.test.model.utils.Randomizer
import com.espn.test.perf.configurations.HttpConfProvider.WebCustomerUrl
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.http.request.builder.HttpRequestBuilder

trait WebApiRequests extends WebCustomerUrl {

  def createCustomerRequest: HttpRequestBuilder = {
    http("Create customer account")
      .post("/jgc/v6/client/ESPN-ONESITE.WEB-PROD/guest/register")
      .queryParamMap(Map(
        "autogeneratePassword" -> "false",
        "autogenerateUsername" -> "true",
        "langPref" -> "en-UK"
      ))
      .body(StringBody("${bodyNewCustomer}"))
      .header("authorization", "APIKEY tHHuadM8QEHidYIvF7eHqH/xKtuNuB7LF+WlRWC3AgqE29asXSZGsDSHyDa6zfhTeZqsCjRvHmVi2qpV9ShuxhuRIDKa")
      .header("content-type", "application/json")
      .check(status.is(200))
  }

  def login: HttpRequestBuilder = {
    http("Login user")
      .post("/jgc/v6/client/ESPN-ONESITE.WEB-PROD/guest/login")
      .queryParamMap(Map(
        "langPref" -> "en-UK"
      ))
      .body(StringBody("""{"loginValue":"${email}","password":"${password}"}"""))
      .header("authorization", "APIKEY tHHuadM8QEHidYIvF7eHqH/xKtuNuB7LF+WlRWC3AgqE29asXSZGsDSHyDa6zfhTeZqsCjRvHmVi2qpV9ShuxhuRIDKa")
      .header("content-type", "application/json")
      .check(status.is(200))

  }

  def getCompetition: HttpRequestBuilder =
    http("Get competitions")
      .get("http://fantasyfc.espnqa.com/uk/17/selectgame.do")
      .check(status.is(200))
      .check(regex("""(?:data-game-link=)"(.*?)"""").findAll.saveAs("competitionsId"))

  def getCategoriesIds: HttpRequestBuilder =
    http("Get categories")
      .get("http://fantasyfc.espnqa.com/CleverTV_espn/xmlapi")
      .queryParamMap(Map(
        "type" -> "game-settings",
        "competitionid" -> "${competitionId}",
        "language" -> "en",
        "outputtype" -> "json"
      ))
    .check(status.is(200))
    .check(jsonPath("$.categories[:].id").findAll.saveAs("getCategoriesId"))
    .check(jsonPath("$.maxPerTeam").saveAs("playersInTeam"))

  def getPlayers: HttpRequestBuilder =
    http("Get Players")
    .get("http://fantasyfc.espnqa.com/CleverTV_espn/xmlapi")
      .queryParamMap(Map(
        "type" -> "playerlistnew",
        "competitionid" -> "${competitionId}",
        "language" -> "en",
        "outputtype" -> "json"
      ))
      .check(status.is(200))
    .check(jsonPath("$.players").find.saveAs("playersGet"))
//    .check(bodyString.saveAs("playersGet"))

  def createTeam: HttpRequestBuilder =
    http("Create Team")
    .get("http://fantasyfc.espnqa.com/CleverTV_espn/xmlapi")
    .queryParamMap(Map(
      "type" -> "saveentry",
      "competitionid" -> "${competitionId}",
      "skinid" -> "27",
      "teamname" -> Randomizer.getAlpha(9),
      "playercodes" -> "${playercodes}",
      "userid" -> "${userId}"
    ))
    .check(status.is(200))
    .check(jsonPath("$.message").is("saved"))


}
