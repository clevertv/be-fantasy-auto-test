package com.espn.test.perf.simulations

import com.espn.test.model.domain.GetPlayersFromWeb
import com.espn.test.model.general.Config
import com.espn.test.perf.configurations.HttpConfProvider.WebCustomerUrl
import io.circe._
import io.circe.parser._
import io.gatling.core.Predef._
import requests.WebApiRequests

import scala.concurrent.duration._
import scala.util.Random

class AddCardsSimulation extends Simulation with WebCustomerUrl with WebApiRequests with Config {

  val dataUsers = jsonFile("./src/main/resources/feed/user.json").circular

  val addCards = scenario("Users add cards")
    .feed(dataUsers)
    .exec(getCompetition)
    .exec(session => {
      val listOfCompetitions = session("competitionsId").as[List[Int]]
      val randomCompetition = listOfCompetitions(Random.nextInt(listOfCompetitions.size)).toString.toInt
      session.set("competitionId", randomCompetition)
    })
    .exec(getCategoriesIds)
    .exec(getPlayers)
    .exec(session => {
      import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}

      implicit val decoder: Decoder[GetPlayersFromWeb] = deriveDecoder[GetPlayersFromWeb]
      implicit val encoder: Encoder[GetPlayersFromWeb] = deriveEncoder[GetPlayersFromWeb]

      val listOfCategories: Seq[Int] = session("getCategoriesId").as[Vector[Int]]

      val listOfPlayersFromWeb = parse(session("playersGet").as[String]).map(_.as[List[GetPlayersFromWeb]]
        .getOrElse(throw new Exception("pla"))).getOrElse(Nil)
        .filter { it =>
          it.code.matches("""(^[a-zA-Z0-9-_])\d\w+""") /*&& listOfCategories.contains(it.categoryId)*/
        }

      val playersInTeam = session("playersInTeam").as[Int]


      val team = listOfPlayersFromWeb.groupBy(_.categoryId).map {
        case (category, listOfGroupedPlayers) => (category -> listOfGroupedPlayers.head.code.toInt)
      }.values.take(playersInTeam)

      session.set("playercodes", team.mkString(":"))
    })
    .exec(createTeam)

  setUp(
    addCards.inject(
      constantUsersPerSec(Simulation.users) during (Simulation.time second) randomized
    )
  )
    .protocols(httpConf)


}
