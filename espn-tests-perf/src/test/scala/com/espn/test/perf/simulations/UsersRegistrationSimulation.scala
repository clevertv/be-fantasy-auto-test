package com.espn.test.perf.simulations


import com.espn.test.perf.configurations.HttpConfProvider.WebCustomerUrl
import io.gatling.core.Predef._
import io.gatling.core.scenario.Simulation
import requests.WebApiRequests
import com.espn.test.perf.dataLoader.DataNewCustomer.feederCreateCustomer


import scala.concurrent.duration._

class UsersRegistrationSimulation extends Simulation with WebCustomerUrl with WebApiRequests{

    val createCustomer = scenario("Create new customer")
      .feed(feederCreateCustomer)
      .exec(createCustomerRequest)


  setUp(
    createCustomer.inject(
      constantUsersPerSec(1) during(10 second) randomized
    )
  )

    .protocols(httpConf)
}
