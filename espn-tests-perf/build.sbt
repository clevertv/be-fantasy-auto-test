import sbt.Keys.libraryDependencies


enablePlugins(GatlingPlugin)

val gatlingVersion = "3.1.3"

libraryDependencies ++= Seq(
  "io.gatling.highcharts" % "gatling-charts-highcharts" % gatlingVersion,
  "io.gatling" % "gatling-test-framework" % gatlingVersion
)

val testOnlyValue: String = sys.props.getOrElse("TESTONLY", default = "com.espn.test.perf.simulations.AddCardsSimulation")
TaskKey[Unit]("ranTest") := {
  (testOnly in Gatling).toTask(s" ${testOnlyValue}").value
}