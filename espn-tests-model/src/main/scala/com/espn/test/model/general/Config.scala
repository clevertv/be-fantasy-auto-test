package com.espn.test.model.general

import java.net.URI

import com.typesafe.config.{ConfigFactory, ConfigRenderOptions, Config => TypesafeConfig}

import scala.util.Try
import com.typesafe.scalalogging.LazyLogging

trait Config extends LazyLogging {

  private val config = ConfigFactory.load
  lazy val cleanedConfig: TypesafeConfig = Seq("akka", "file", "awt", "line", "os", "path", "sun", "java", "user").foldLeft(config) {
    case (cfg, path) => cfg.withoutPath(path)
  }
  if (Try(config.getBoolean("DEBUG_CONFIG")).toOption.getOrElse(false)) {
    logger.warn("Config debug info: {}", cleanedConfig.root().render(ConfigRenderOptions.defaults().setComments(false)))
  }

  object env {
    val _env = config.getString("envURL")
    private val _confEnv = config.getConfig("env")
    val universe = _confEnv.getString("universe")
  }


  object dataLoader {
    private val _dataLoader = config.getConfig("dataLoader")
    val numberOfCreateUsers: Int = _dataLoader.getInt("numberOfCreateUsers")
    val interOrExternUsers: Boolean = _dataLoader.getBoolean("createUser")

    //variables for create game
    private val _gameCretaeData = _dataLoader.getConfig("gameCreate")
    val numberOfSideForGame: Int = _gameCretaeData.getInt("numberOfSide")
    val numberOfCategoryForGame: Int = _gameCretaeData.getInt("numberOfCategory")
    val numberOfPlayerForGame: Int = _gameCretaeData.getInt("numberOfPlayer")
    val numberOfStatisticsPeriodForGame: Int = _gameCretaeData.getInt("numberOfStatisticsPeriod")
    val numberOfRuleForGame: Int = _gameCretaeData.getInt("numberOfRule")

    //variables for create competition
    private val _competitionsCreateData = _dataLoader.getConfig("competitionCreate")
    val numberOfLegendForCompetition: Int = _competitionsCreateData.getInt("numberOfLegend")
    val numberOfActivationDateForCompetition: Int = _competitionsCreateData.getInt("numberOfActivationDate")
    val numberOfPartialPrizeForCompetition: Int = _competitionsCreateData.getInt("numberOfPartialPrize")
    val numberOfTransferPeriodForCompetition: Int = _competitionsCreateData.getInt("numberOfTransferPeriod")

  }

  object Simulation {
    private val _simulation = config.getConfig("simulation")
    val users = _simulation.getInt("users")
    val time = _simulation.getInt("during")
  }

  class Endpoint(val url: String) {
    private val uriObject = new URI(url)
    val host: String = uriObject.getHost
    val port: Int = Option(uriObject.getPort).filter(_ > 0).getOrElse(if (uriObject.getScheme == "https") 433 else 80)
    val scheme: String = uriObject.getScheme
    val path: String = uriObject.getPath
  }

}
