package com.espn.test.model.utils

import java.text.SimpleDateFormat
import java.time.format.DateTimeFormatter
import java.time.temporal.TemporalAccessor
import java.time.{Instant, LocalTime, ZoneOffset, ZonedDateTime}
import java.util.{Date, UUID}

import org.joda.time.DateTimeZone

import scala.io.Source
import scala.util.Random

object Randomizer {

  val random = new Random()

  private lazy val firstNames = Source.fromInputStream(getClass.getResourceAsStream("/random/first-names.txt")).getLines().toList
  private lazy val lastNames = Source.fromInputStream(getClass.getResourceAsStream("/random/last-names.txt")).getLines().toList
  private lazy val cities = Source.fromInputStream(getClass.getResourceAsStream("/random/cities.txt")).getLines().toList
  private lazy val categories = Source.fromInputStream(getClass.getResourceAsStream("/random/category")).getLines().toList
  private val currencies = List("GBP")

  //---------- Randoms from lists ----------

  def randomFromList[T](values: List[T]): T = values(random.nextInt(values.size))

  def firstName: String = randomFromList(firstNames)

  def lastName: String = randomFromList(lastNames)

  def city: String = randomFromList(cities)

  def currency: String = randomFromList(currencies)

  def nameCategory: String = randomFromList(categories)

  //---------- Private randomizers ----------

  private def varLength(min: Int = 10, max: Int = 20) = random.nextInt(max - min) + min

  private def randomStringFromCharList(length: Int, chars: Seq[Char]): String = {
    Iterator.continually(chars(random.nextInt(chars.length))).take(length).mkString
  }

  //---------- Basic randomizers ----------

  def uuid(): String = UUID.randomUUID().toString

  def id(): String = {
    val prefix = new SimpleDateFormat("HHmmss_").format(new Date())
    prefix + uuid()
  }

  def boolean: Boolean = random.nextBoolean()

  def int(min: Int = 0, max: Int = 100): Int = {
    random.nextInt(max - min + 1) + min
  }

  def double(min: Double = 0.0, max: Double = 100.0): Double = {
    BigDecimal(random.nextFloat() * (max - min) + min).setScale(2, BigDecimal.RoundingMode.HALF_DOWN).toDouble
  }

  def getAlpha(length: Int): String = {
    val chars = ('a' to 'z') ++ ('A' to 'Z')
    randomStringFromCharList(length, chars)
  }

  def getAlphanumeric(length: Int): String = random.alphanumeric.take(length).mkString

  object dateTime {

    def randomTime(): String = {
      DateTimeFormatter.ofPattern("HH:mm").format(LocalTime.of(random.nextInt(24), random.nextInt(60)))
    }

    def randomTimeZoneId(): String = {
      val allTimeZoneIds = DateTimeZone.getAvailableIDs.toArray
      allTimeZoneIds(Randomizer.int(0, allTimeZoneIds.size)).toString
    }

    object ISO8601 {
      def format(temporalAccessor: TemporalAccessor): String = {
        DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm").withZone(ZoneOffset.UTC).format(temporalAccessor)
      }

      def formatCom(temporalAccessor: TemporalAccessor): String = {
        DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").withZone(ZoneOffset.UTC).format(temporalAccessor)
      }

      def yesterday(): String = format(ZonedDateTime.now().minusDays(1))

      def now(): String = format(Instant.now())

      def tomorrow(): String = format(ZonedDateTime.now().plusDays(1))

      def nowPlus(plusDays: Int): String = format(ZonedDateTime.now().plusDays(plusDays))
    }

  }

}
