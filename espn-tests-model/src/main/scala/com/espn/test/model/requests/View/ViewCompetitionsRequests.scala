package com.espn.test.model.requests.View

import com.espn.test.model.Specifications
import io.restassured.RestAssured.`given`
import io.restassured.module.scala.RestAssuredSupport.AddThenToResponse

object ViewCompetitionsRequests {

  private val _getIgGame = "/CleverTV_espn/admin/adminJspViewCompetitions.do"

  def getIdGames(): Any = {
    given()
      .spec(Specifications.createSimpleBuilderRequest)
      //      .relaxedHTTPSValidation()
      .cookie("JSESSIONID=7D825051845558D6C41927CED8A2E0AF.stag-admin; SWID=9eb01b7c-928b-422d-9eb4-fbf013caca76; ctv_cookie_accept=true; JSESSIONID=7D825051845558D6C41927CED8A2E0AF.stag-worker")
//      .contentType("text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3")
      .when()
      .get(_getIgGame)
      .Then()
      .statusCode(200)
      .contentType("text/html;charset=UTF-8")
      .extract()
      .response().getBody.print()
  }
}
