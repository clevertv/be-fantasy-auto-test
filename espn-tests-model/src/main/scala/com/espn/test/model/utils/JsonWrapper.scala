package com.espn.test.model.utils

import java.io.File
import com.jayway.jsonpath.JsonPath
import org.json4s._
import org.json4s.jackson.JsonMethods._
import org.json4s.jackson.Serialization.write

class JsonWrapper(obj: AnyRef) {

  lazy val document = {
    obj match {
      case _: String => JsonPath.parse(obj.asInstanceOf[String])
      case _: File => JsonPath.parse(obj.asInstanceOf[File])
      case _: AnyRef => JsonPath.parse(objectToString(obj))
    }
  }

  private def objectToString(obj: AnyRef): String = {
    implicit val formats = DefaultFormats
    write(obj)
  }

  def setField(path: String, value: Any): JsonWrapper = {
    document.set(path, value)
    this
  }

  def readField[T](path: String): T = {
    document.read[T](path)
  }

  def hasField(path: String): Boolean = {
    try {
      document.read(path)
      true
    } catch {
      case e: Exception => false
    }
  }

  def removeField(path: String): JsonWrapper = {
    document.delete(path)
    this
  }

  def putValue(root: String, key: String, value: AnyRef): JsonWrapper = {
    document.put(root, key, value)
    this
  }

  def asJsonString: String = {
    document.jsonString()
  }

  def asPrettyJsonString: String = {
    pretty(parse(document.jsonString()))
  }

}
