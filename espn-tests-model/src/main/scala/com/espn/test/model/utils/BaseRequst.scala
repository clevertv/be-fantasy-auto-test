package com.espn.test.model.utils

import io.restassured.http.{Header, Headers}
import io.restassured.module.scala.RestAssuredSupport.AddThenToResponse
import io.restassured.response.{Response, ResponseBodyExtractionOptions}

import scala.reflect._

class BaseRequst  {

  def getResponseData(response: Response) = {
    ResponseData(response.Then().extract().body(), response.getStatusCode, response.getHeaders)
  }
}

case class ResponseData(body: ResponseBodyExtractionOptions, status: Int, headers: Headers) {
  lazy val bodyAsString = body.asString
  lazy val isSuccess = status == 200 || status == 201

  def getBodyAs[T: ClassTag]: T = body.as(classTag[T].runtimeClass).asInstanceOf[T]

  def getHeader(name: String): Option[Header] = Option(headers.get(name))

  def getAs[T: ClassTag] = (getBodyAs[T], status, headers)

  def checkStatus(expected: Int) = {
    assert(status == expected, s"Invalid HTTP status code, expected $expected but got $status")
    this
  }

  def checkStatus(expected: Seq[Int] = Seq(200, 201, 202, 203, 204)) = {
    assert(expected.contains(status), s"Invalid HTTP status code, expected one of $expected but got $status")
    this
  }
}