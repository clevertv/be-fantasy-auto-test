package com.espn.test.model.domain.uploads

case class Game(
                 gameName: String,
                 playersInTeam: String,
                 teamValue: String,
                 showSides: String,
                 fluctuateValuesUsingFacts: String,
                 side: Array[Side],
                 category: Array[Category],
                 player: Array[PlayerInGame],
                 statisticsPeriod: Array[StatisticsPeriod]
               ) extends SelectCom

case class Side(
                 sideName: String,
                 sideAbbreviation: String,
                 info1: String
               )

case class Category(
                   name: String,
                   abbreviation: String,
                   rule: Array[Rule]
                   )

case class Rule(
                 description: String,
                 abbreviation: String,
                 points: String,
                 statsPeriodDisplay: String,
                 specialRule: String,
                 orderIndex: String,
               )

case class PlayerInGame(
                       Info1: String,
                       name: String,
                       code: String,
                       side: String,
                       category: String,
                       value: String
                       )

case class Info1(
                  info1: String
                )

case class StatisticsPeriod(
                           name: String,
                           startDate: String,
                           endDate: String,
                           freeTransfers: String,
                           penaltyPoints: String
                           )
