package com.espn.test.model.domain.uploads

case class Players(
                  gameName: String,
                  player: Seq[Player]
                  ) extends SelectCom

case class Player(
                 name: String,
                 code: String,
                 side: String,
                 category: String,
                 value: String,
                 shirtNumber: String,
                 info1: Option[String]
                 )
