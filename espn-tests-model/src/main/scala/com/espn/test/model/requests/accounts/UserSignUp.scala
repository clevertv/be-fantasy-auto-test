package com.espn.test.model.requests.accounts

import com.espn.test.model.Specifications
import com.espn.test.model.domain.accounts.{CreateAccounts, ReceivedUserAccount}
import com.espn.test.model.utils.Randomizer
import io.restassured.RestAssured.`given`
import io.restassured.module.scala.RestAssuredSupport.AddThenToResponse

object UserSignUp {

  private val _signUp = "/jgc/v6/client/ESPN-ONESITE.WEB-PROD/guest/register"
  private val _signUpWithoutEspn = "/CleverTV_espn/jsonapi"

  def postCreateUser(userBody: CreateAccounts) = {
    given()
//      .spec(Specifications.registrations)
      .queryParam("autogeneratePassword", "false")
      .queryParam("autogenerateUsername", "true")
      .queryParam("langPref", "en-UK")
      .header("Authorization", "APIKEY tHHuadM8QEHidYIvF7eHqH/xKtuNuB7LF+WlRWC3AgqE29asXSZGsDSHyDa6zfhTeZqsCjRvHmVi2qpV9ShuxhuRIDKa")
      .header("content-type", "application/json")
      .body(userBody)
      .when()
      .post("https://registerdisney.go.com/jgc/v6/client/ESPN-ONESITE.WEB-PROD/guest/register")
      .Then()
      .assertThat().statusCode(200)
  }

  def getCreateUserNotEspn(email: String, password: String, skinName: String = "fantasy")={
    given()
      .spec(Specifications.createSimpleBuilderRequest)
      .relaxedHTTPSValidation()
      .queryParams(
        "type","create-user",
        "clientToken","2_espn",
        "externalId", "123",
        "email", email,
        "password", password,
        "dob","1960-01-20",
        "firstName", Randomizer.firstName,
        "lastName", "Randomizer",
        "Gender", "M",
        "displayName", "LuckyP",
        "houseName", Randomizer.getAlpha(6),
        "street", "undevapeaici",
        "address1", "nuam",
        "address2", "nuam2",
        "town", "ceamaifrumoasa",
        "postCode", "MD1111",
        "country", "test",
        "phoneNumber", "12589460",
        "favourite_team", "2222",
        "skinName", s"$skinName"
      )
      .when()
      .get(_signUpWithoutEspn)
      .Then()
      .assertThat().statusCode(200)
      .extract()
      .body().as(classOf[ReceivedUserAccount])
  }

}
