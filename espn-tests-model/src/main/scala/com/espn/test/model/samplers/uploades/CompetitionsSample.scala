package com.espn.test.model.samplers.uploades

import com.espn.test.model.domain.uploads.{TransferPeriod, _}
import com.espn.test.model.utils.{Randomizer, SaveFile}

object CompetitionsSample extends SaveFile {

  def crearteCompetition(nrOfLegend: Int,
                         nrOfActivationDate: Int,
                         nrOfPartialPrize: Int,
                         nrOfTransferPeriod: Int,
                         gameData: Game
                        ) = {


    val p = new scala.xml.PrettyPrinter(10000, 4)

    var leagueArray = Array[String]()
    var activationDataArray = Array[String]()
    var partialPrize = Array[PartialPrize]()
    var transferPeriod = Array[TransferPeriod]()
    var competitionCategoryArray = Array[CompetitionCategory]()

    val competitionName = {
      Randomizer.getAlpha(20)
    }

    for (l <- 0 until nrOfLegend) {
      leagueArray = leagueArray :+ Randomizer.firstName
    }

    for (a <- 0 until nrOfActivationDate) {
      a match {
        case 0 => activationDataArray = activationDataArray :+ Randomizer.dateTime.ISO8601.now()
        case _ => activationDataArray = activationDataArray :+ Randomizer.dateTime.ISO8601.nowPlus(a + 1)
      }
    }

    for (pp <- 0 until  nrOfPartialPrize) {
      val addPartialPrize = new PartialPrize(
        name = "Round " + pp,
        startDate = activationDataArray(pp),
        endDate = Randomizer.dateTime.ISO8601.nowPlus(pp),
        subType = "week"
      )
      partialPrize = partialPrize :+ addPartialPrize
    }

    for (tr <- 1 to nrOfTransferPeriod) {
      val transferPeriodU = new TransferPeriod(
        maxTransfers = "99999",
        includeInOverallLimit = "N",
        startDate = Randomizer.dateTime.ISO8601.nowPlus(100),
        endDate = Randomizer.dateTime.ISO8601.nowPlus(150),
        transferCost = Randomizer.double(0.0, 0.99).toString,
        maximumCaptainChanges = "99999"
      )
      transferPeriod = transferPeriod :+ transferPeriodU
    }

    for (c <- gameData.category.indices) {
      val categoryCom = new CompetitionCategory(
        name = gameData.category(c).name,
        minimumInSquad = Randomizer.int(0, 2).toString,
        maximumInSquad = Randomizer.int(0, 2).toString,
        minimumInTeam = Randomizer.int(0, 2).toString,
        maximumInTeam = Randomizer.int(0, 2).toString,
        defaultInTeam = Randomizer.int(0, 2).toString,
        minimumCaptainCount = Randomizer.int(0, 2).toString,
        maximumCaptainCount = Randomizer.int(0, 2).toString
      )
      competitionCategoryArray = competitionCategoryArray :+ categoryCom
    }


    val competition = <Competition clientName="espn"
                                   gameName={gameData.gameName}
                                   competitionName={competitionName}
                                   registrationStartDate={Randomizer.dateTime.ISO8601.now()}
                                   registrationEndDate={Randomizer.dateTime.ISO8601.nowPlus(90)}
                                   maximumEntriesPerUser={Randomizer.int(1, 10).toString}
                                   entryCost="0"
                                   cupCost="0"
                                   premiumSurcharge="0"
                                   currency={Randomizer.currency}
                                   freeQualification={Randomizer.int(1, 9).toString}
                                   freeGames={Randomizer.int(1, 9).toString}
                                   freeDiscount={Randomizer.int(0, 9).toString}
                                   globalLeagueName="overall"
                                   statisticsPeriodLeague="N"
                                   randomLeagueName=""
                                   duplicateTeamNames="Y"
                                   defaultLeagueType="h2h">
      <AffinityLeague>
        {leagueArray.map(l =>
        <League>{l}</League>
      )}
      </AffinityLeague>

      <FantasyCompetition minimumPlayersPerSide="0"
                          maximumPlayersPerSide="8"
                          maximumTransfers="99999"
                          minimumTransferCost="0.00"
                          unlimitedTransfersCost="0.00"
                          playersInSquad="8"
                          fluctuatingPlayerValues="N"
                          captainCount="1"
                          maximumCaptainChanges={Randomizer.int(100, 6000).toString}
                          unlimitedSMSCost="0.00"
                          autoSubs="N">

        {activationDataArray.map(a =>
        <ActivationDate>{a}</ActivationDate>
      )}<OverallPrize name="Overall League"/>{partialPrize.map(p => {
          <PartialPrize name={p.name}
                        startDate={p.startDate}
                        endDate={p.endDate}
                        subType={p.subType}/>
      })}{transferPeriod.map(t => {
          <TransferPeriod maxTransfers={t.maxTransfers}
                          includeInOverallLimit={t.includeInOverallLimit}
                          startDate={t.startDate}
                          endDate={t.endDate}
                          transferCost={t.transferCost}
                          maximumCaptainChanges={t.maximumCaptainChanges}/>
      }
      )}{competitionCategoryArray.map(c => {
          <Category name={c.name}
                    minimumInSquad={c.minimumInSquad}
                    maximumInSquad={c.maximumInSquad}
                    minimumInTeam={c.minimumInTeam}
                    maximumInTeam={c.maximumInTeam}
                    defaultInTeam={c.defaultInTeam}
                    minimumCaptainCount={c.minimumCaptainCount}
                    maximumCaptainCount={c.maximumCaptainCount}/>

      })}

      </FantasyCompetition>
    </Competition>

    val competitionXML = p.format(competition)

    saveToFile(s"Competition $competitionName", competitionXML)
    (competitionName, competitionXML)
  }

}
