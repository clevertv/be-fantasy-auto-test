package com.espn.test.model.utils

import java.nio.charset.StandardCharsets
import java.nio.file.{Files, Paths}

trait WriteInFile {

  def saveToFile(fileName: String, data: String, typeFile: String ): Unit = {

    Files.write(Paths.get(s"""./espn-tests-perf/src/main/resources/feed/$fileName.$typeFile"""), data.getBytes(StandardCharsets.UTF_8))
  }

}
