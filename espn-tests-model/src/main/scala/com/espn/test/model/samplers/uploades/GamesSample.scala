package com.espn.test.model.samplers.uploades

import com.espn.test.model.domain.uploads._
import com.espn.test.model.utils.Randomizer
import com.espn.test.model.utils.SaveFile
import scala.collection.parallel.ParSeq
import scala.xml.Elem

object GamesSample extends SaveFile {

  def createGame(
                  nrOfSide: Int,
                  nrOfCategory: Int,
                  nrOfPlayer: Int,
                  nrOfStatisticsPeriod: Int,
                  nrOfRule: Int): (String, String) = {

    val p = new scala.xml.PrettyPrinter(10000, 4)

    var sideArray = Array[Side]()
    var categoryArray = Array[Category]()
    var playersArray = Array[PlayerInGame]()
    var statisticsPerionArray = Array[StatisticsPeriod]()

    var inf = new String


    //create sideArray
    //    ParSeq(0 to nrOfSide: _*).foreach(s => {
    for (s <- 0 to nrOfSide) {
      inf = s.toString
      val side: Side = new Side(
        sideName = Randomizer.firstName + s,
        sideAbbreviation = "AAA" + s,
        info1 = inf
      )
      sideArray = sideArray :+ side
    }

    ParSeq(0 to nrOfCategory: _*).foreach(c => {
      val category: Category = new Category(
        name = Randomizer.nameCategory,
        abbreviation = "abbTest" + c,
        rule = createRule(nrOfRule)
      )
      categoryArray = categoryArray :+ category
    })

    ParSeq(0 to nrOfPlayer: _*).foreach(p => {
      val player: PlayerInGame = new PlayerInGame(
        Info1 = new Info1(
          info1 = "CEva " + p
        ).toString,
        name = Randomizer.firstName,
        code = Randomizer.getAlphanumeric(30),
        side = sideArray(Randomizer.int(0, sideArray.length - 1)).sideName,
        category = categoryArray(Randomizer.int(0, categoryArray.length - 1)).name,
        value = "1"
      )
      playersArray = playersArray :+ player
    })

    //    ParSeq(0 to nrOfStatisticsPeriod: _*).foreach(s =>{
    for (s <- 0 to nrOfStatisticsPeriod) {
      val statistics: StatisticsPeriod = new StatisticsPeriod(
        name = "Round " + s,
        startDate = Randomizer.dateTime.ISO8601.nowPlus(s),
        endDate = Randomizer.dateTime.ISO8601.nowPlus(Randomizer.int(10, 20)),
        freeTransfers = "99999",
        penaltyPoints = "0"
      )
      statisticsPerionArray = statisticsPerionArray :+ statistics
    } //)

    val game = new Game(
      gameName = Randomizer.getAlpha(10) + " 2019-20",
      playersInTeam = "8",
      teamValue = Randomizer.int(10, 200).toString,
      showSides = "Y",
      fluctuateValuesUsingFacts = "N",
      side = sideArray,
      category = categoryArray,
      player = playersArray,
      statisticsPeriod = statisticsPerionArray
    )

    val gameBody: Elem = <Game gameName={game.gameName}
                               playersInTeam={game.playersInTeam}
                               teamValue={game.teamValue}
                               showSides={game.showSides}
                               fluctuateValuesUsingFacts={game.fluctuateValuesUsingFacts}>
      {game.side.map(s => {
          <Side sideName={s.sideName}
                sideAbbreviation={s.sideAbbreviation}
                info1={s.info1}/>
      })}{game.category.map(c => {
        <Category name={c.name}
                  abbreviation={c.abbreviation}>
          {c.rule.map(r => {
            <Rule description={r.description}
                  abbreviation={r.abbreviation}
                  statsPeriodDisplay={r.statsPeriodDisplay}
                  specialRule={r.specialRule}
                  orderIndex={r.orderIndex}
                  points={r.points}/>
        })}
        </Category>
      })}{game.player.map(p => {
        <Player name={p.name}
                code={p.code}
                side={p.side}
                category={p.category}
                value={p.value}>
          <Info1>
            {p.Info1}
          </Info1>
        </Player>
      })}{game.statisticsPeriod.map(s => {
          <StatisticsPeriod name={s.name}
                            startDate={s.startDate}
                            endDate={s.endDate}
                            freeTransfers={s.freeTransfers}
                            penaltyPoints={s.penaltyPoints}/>
      })}

    </Game>


    val gameXML = p.formatNodes(gameBody)
    saveToFile("Game", gameXML )

    (game.gameName ,gameXML)
  }

  def createRule(nrRule: Int): Array[Rule] = {
    var ruleArray = Array[Rule]()
    ParSeq(0 to nrRule: _*).foreach(f = r => {
      val rule: Rule = new Rule(
        description = Randomizer.getAlpha(15),
        abbreviation = Randomizer.getAlpha(3) + r,
        points = Randomizer.int(-5, 25).toString,
        statsPeriodDisplay = "N",
        specialRule = "Y",
        orderIndex = "9"
      )
      ruleArray = ruleArray :+ rule
    })
    ruleArray
  }
}

