package com.espn.test.model

import com.espn.test.model.general.Config
import com.espn.test.model.utils.Randomizer
import io.restassured.builder.RequestSpecBuilder
import io.restassured.specification

object Specifications extends Config{

  def createSimpleBuilderRequest: specification.RequestSpecification =
    buildRequestSpecification(env._env)

  private def buildRequestSpecification(
                                       url: String,
                                       //contentType: String = "text/html; charset=utf-8 ",
                                       mediaTypes: String = "application/json, text/plain"
                                       ) = {
    new RequestSpecBuilder()
      .setBaseUri(url)
      //.setContentType(contentType)
      .setAccept(mediaTypes)
      .addHeader("X-Client-Req-Id", Randomizer.uuid())
      .build()
  }

}
