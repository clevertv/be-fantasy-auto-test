package com.espn.test.model.xst

import com.espn.test.model.domain.uploads._
import com.espn.test.model.general.BaseSuit

trait xstConfig extends BaseSuit {

  def competitionsXST(): Unit = {
    val competitionFields = classOf[Competition]
    val affinityLeagueFields = classOf[AffinityLeague]
    val fantasyCompetitionFields = classOf[FantasyCompetition]
    val categoryFileds = classOf[CompetitionCategory]
    val transferPeriodFields = classOf[TransferPeriod]
    val partialPrizeFields = classOf[PartialPrize]
    val overallPrizeFields = classOf[OverallPrize]



    //create competition sections
    xst.useAttributeFor(competitionFields, "clientName")
    xst.useAttributeFor(competitionFields, "gameName")
    xst.useAttributeFor(competitionFields, "competitionName")
    xst.useAttributeFor(competitionFields, "registrationStartDate")
    xst.useAttributeFor(competitionFields, "registrationEndDate")
    xst.useAttributeFor(competitionFields, "maximumEntriesPerUser")
    xst.useAttributeFor(competitionFields, "entryCost")
    xst.useAttributeFor(competitionFields, "cupCost")
    xst.useAttributeFor(competitionFields, "premiumSurcharge")
    xst.useAttributeFor(competitionFields, "currency")
    xst.useAttributeFor(competitionFields, "freeQualification")
    xst.useAttributeFor(competitionFields, "freeGames")
    xst.useAttributeFor(competitionFields, "freeDiscount")
    xst.useAttributeFor(competitionFields, "globalLeagueName")
    xst.useAttributeFor(competitionFields, "statisticsPeriodLeague")
    xst.useAttributeFor(competitionFields, "randomLeagueName")
    xst.useAttributeFor(competitionFields, "duplicateTeamNames")

    xst.alias("Competition", classOf[Competition])
    //    xst.addImplicitCollection(classOf[AffinityLeague], "league")
    xst.addImplicitArray(classOf[AffinityLeague], "league", "League")
    //    xst.addImplicitCollection(classOf[Competition], "fantasyCompetition")

    //create AffinityLeague
    xst.useAttributeFor(affinityLeagueFields, "league")

    //create fantasy competition
    xst.useAttributeFor(fantasyCompetitionFields, "minimumPlayersPerSide")
    xst.useAttributeFor(fantasyCompetitionFields, "maximumPlayersPerSide")
    xst.useAttributeFor(fantasyCompetitionFields, "maximumPlayersPerSide")
    xst.useAttributeFor(fantasyCompetitionFields, "maximumTransfers")
    xst.useAttributeFor(fantasyCompetitionFields, "minimumTransferCost")
    xst.useAttributeFor(fantasyCompetitionFields, "playersInSquad")
    xst.useAttributeFor(fantasyCompetitionFields, "unlimitedTransfersCost")
    xst.useAttributeFor(fantasyCompetitionFields, "fluctuatingPlayerValues")
    xst.useAttributeFor(fantasyCompetitionFields, "captainCount")
    xst.useAttributeFor(fantasyCompetitionFields, "maximumCaptainChanges")
    xst.useAttributeFor(fantasyCompetitionFields, "unlimitedSMSCost")
    xst.useAttributeFor(fantasyCompetitionFields, "autoSubs")
    xst.addImplicitCollection(classOf[FantasyCompetition], "Category")
    xst.addImplicitCollection(classOf[FantasyCompetition], "TransferPeriod")
    xst.alias("PartialPrize", classOf[PartialPrize])
    xst.addImplicitCollection(classOf[FantasyCompetition], "PartialPrize")
    //    xst.addImplicitCollection(classOf[FantasyCompetition], "overallPrize")
    xst.addImplicitArray(classOf[FantasyCompetition], "ActivationDate", "ActivationDate")

    //create Competition Category
    xst.useAttributeFor(categoryFileds, "name")
    xst.useAttributeFor(categoryFileds, "minimumInSquad")
    xst.useAttributeFor(categoryFileds, "maximumInSquad")
    xst.useAttributeFor(categoryFileds, "minimumInTeam")
    xst.useAttributeFor(categoryFileds, "maximumInTeam")
    xst.useAttributeFor(categoryFileds, "defaultInTeam")
    xst.useAttributeFor(categoryFileds, "minimumCaptainCount")
    xst.useAttributeFor(categoryFileds, "maximumCaptainCount")
    xst.alias("Category", classOf[CompetitionCategory])

    //create Transfer Period
    xst.useAttributeFor(transferPeriodFields, "maxTransfers")
    xst.useAttributeFor(transferPeriodFields, "includeInOverallLimit")
    xst.useAttributeFor(transferPeriodFields, "startDate")
    xst.useAttributeFor(transferPeriodFields, "endDate")
    xst.useAttributeFor(transferPeriodFields, "transferCost")
    xst.useAttributeFor(transferPeriodFields, "maximumCaptainChanges")

    //create partial prize
    xst.useAttributeFor(partialPrizeFields, "name")
    xst.useAttributeFor(partialPrizeFields, "startDate")
    xst.useAttributeFor(partialPrizeFields, "endDate")
    xst.useAttributeFor(partialPrizeFields, "subType")

    //create overall prize
    xst.useAttributeFor(overallPrizeFields, "name")
  }

  def plaeyerXST(): Unit = {

    competitionsXST()
    val playerFields = classOf[Player]
    val playersFields = classOf[Players]

    xst.useAttributeFor(playersFields, "gameName")

    xst.useAttributeFor(playerFields, "name")
    xst.useAttributeFor(playerFields, "code")
    xst.useAttributeFor(playerFields, "side")
    xst.useAttributeFor(playerFields, "category")
    xst.useAttributeFor(playerFields, "value")
    xst.useAttributeFor(playerFields, "shirtNumber")

    xst.alias("Players", classOf[Players])
    xst.alias("Player", classOf[Player])
    xst.addImplicitCollection(classOf[Players], "player")
  }

  def gameXST(): Unit = {
    val gameClass = classOf[Game]
    val sideClass = classOf[Side]
    val categoryClass = classOf[Category]
    val playerInGameClass = classOf[PlayerInGame]
    val statisticsPeriodClass = classOf[StatisticsPeriod]
    val ruleClass = classOf[Rule]
    val info1Class = classOf[Info1]

    xst.useAttributeFor(info1Class, "info1")

    xst.useAttributeFor(gameClass, "gameName")
    xst.useAttributeFor(gameClass, "playersInTeam")
    xst.useAttributeFor(gameClass, "teamValue")
    xst.useAttributeFor(gameClass, "showSides")
    xst.useAttributeFor(gameClass, "fluctuateValuesUsingFacts")

    xst.useAttributeFor(sideClass, "sideName")
    xst.useAttributeFor(sideClass, "sideAbbreviation")
    xst.useAttributeFor(sideClass, "info1")

    xst.useAttributeFor(categoryClass, "name")
    xst.useAttributeFor(categoryClass, "abbreviation")

    xst.useAttributeFor(ruleClass, "description")
    xst.useAttributeFor(ruleClass, "abbreviation")
    xst.useAttributeFor(ruleClass, "points")
    xst.useAttributeFor(ruleClass, "statsPeriodDisplay")
    xst.useAttributeFor(ruleClass, "specialRule")
    xst.useAttributeFor(ruleClass, "orderIndex")

//    xst.useAttributeFor(playerInGameClass, "Info1")
//    xst.addImplicitArray(classOf[FantasyCompetition], "Info1")
////    xst.registerConverter()
    xst.useAttributeFor(playerInGameClass, "name")
    xst.useAttributeFor(playerInGameClass, "code")
    xst.useAttributeFor(playerInGameClass, "side")
    xst.useAttributeFor(playerInGameClass, "category")
    xst.useAttributeFor(playerInGameClass, "value")

    xst.useAttributeFor(statisticsPeriodClass, "name")
    xst.useAttributeFor(statisticsPeriodClass, "startDate")
    xst.useAttributeFor(statisticsPeriodClass, "endDate")
    xst.useAttributeFor(statisticsPeriodClass, "freeTransfers")
    xst.useAttributeFor(statisticsPeriodClass, "penaltyPoints")

    xst.alias("Game", classOf[Game])
    xst.alias("Category", classOf[Category])
    xst.alias("Rule", classOf[Rule])
    xst.alias("Player", classOf[PlayerInGame])
    xst.alias("StatisticsPeriod", classOf[StatisticsPeriod])
    xst.alias("Side", classOf[Side])
    xst.alias("Info1", classOf[Info1])

    xst.addImplicitCollection(classOf[Game], "side")
    xst.addImplicitCollection(classOf[Game], "category")
    xst.addImplicitCollection(classOf[Category], "rule")
    xst.addImplicitCollection(classOf[Game], "player")
    xst.addImplicitCollection(classOf[Game], "statisticsPeriod")
//        xst.addImplicitCollection(info1Class, "info1")

  }
}
