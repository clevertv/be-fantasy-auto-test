package com.espn.test.model.samplers.uploades

import com.espn.test.model.domain.uploads.{Game, Player, Players}
import com.espn.test.model.utils.{Randomizer, SaveFile}


object PlayersSample extends SaveFile{

  def createPlayersUpdate(gameData: Game): String = {

    val prettyPrinter = new scala.xml.PrettyPrinter(10000, 4)

    val side = gameData.side


    var player: Array[Player] = Array[Player]()

    side.foreach(s =>{
        for(p <- 1 to 21){
          val playerCreate = Player(
            name = Randomizer.getAlpha(1)+"."+Randomizer.getAlpha(10),
            code = Randomizer.int(1000, 99999).toString,
            side = s.sideName,
            category = gameData.category(Randomizer.int(0, gameData.category.length-1)).name,
            value = "1",
            shirtNumber = Randomizer.int(1, 99).toString,
            info1 = Some(Randomizer.getAlpha(20))
          )
          player = player :+ playerCreate
        }
    } )

    val players = Players(
      gameName = gameData.gameName,
      player = player
    )

    val playerMessage = <Players gameName={players.gameName}>
      {players.player.map(p => {
        <Player name={p.name}
                code={p.code}
                side={p.side}
                category={p.category}
                value={p.value}
                shirtNumber={p.shirtNumber}><Info1>{p.info1}</Info1></Player>
      })}

    </Players>

    val playersXML = prettyPrinter.format(playerMessage)

    saveToFile(s"Players", playersXML)
    playersXML
  }
}
