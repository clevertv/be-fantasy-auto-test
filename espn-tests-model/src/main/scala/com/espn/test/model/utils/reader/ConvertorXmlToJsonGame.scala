package com.espn.test.model.utils.reader

import com.espn.test.model.domain.uploads._

import scala.xml.{Elem, NodeSeq}

trait ConvertorXmlToJsonGame {

//  def toJsonObject(xmlOb: NodeSeq): ListBuffer[Game] = {
  def toJsonObject(xmlOb: NodeSeq): Array[SelectCom] = {

    val gameAttr = xmlOb.asInstanceOf[Elem]

    var listOfGame = Array[SelectCom]()

    gameAttr.foreach { data =>
      var sideList =  Array[Side]()
      var categoryList = Array[Category]()
      var playerList = Array[PlayerInGame]()
      var statisticList = Array[StatisticsPeriod]()

      val sides = data \ "Side"
      sides.foreach { side =>
        var sideD = new Side(
          sideName = side \@ "sideName",
          sideAbbreviation = side \@ "sideAbbreviation",
          info1 = side \@ "info1"
        )
        sideList = sideList :+ sideD
      }

      val categories = data \ "Category"
      categories.foreach { category =>
        var ruleList = Array[Rule]()
        var categoryD =  Category(
          name = category \@ "name",
          abbreviation = category \@ "abbreviation",
          rule = ruleList
        )
        val rules = category \ "Rule"
        rules.foreach { rule =>
          var ruleD =  Rule(
            description = rule \@ "description",
            abbreviation = rule \@ "abbreviation",
            points = rule \@ "points",
            statsPeriodDisplay = rule \@ "statsPeriodDisplay",
            specialRule = rule \@ "specialRule",
            orderIndex = rule \@ "orderIndex"
          )
          ruleList = ruleList :+ ruleD
        }

        categoryList = categoryList :+ categoryD
      }

      val players = data \ "Player"
      players.foreach { player =>
        var playerD = new PlayerInGame(
          value = player \@ "value",
          name = player \@ "name",
          code = player \@ "code",
          side = player \@ "code",
          category = player \@ "category",
          Info1 = player.child(1).text
        )
        playerList = playerList :+ playerD

      }

      val statisticsPeriods = data \ "StatisticsPeriod"
      statisticsPeriods.foreach { statistic =>
        val statisticD = new StatisticsPeriod(
          name = statistic \@ "name",
          startDate = statistic \@ "startDate",
          endDate = statistic \@ "endDate",
          freeTransfers = statistic \@ "freeTransfers",
          penaltyPoints = statistic \@ "penaltyPoints"
        )
        statisticList = statisticList :+ statisticD
      }

      var listOfGame1 = new Game(
        gameAttr.attribute("gameName").get.toString(),
        gameAttr.attribute("playersInTeam").get.toString(),
        gameAttr.attribute("teamValue").get.toString(),
        gameAttr.attribute("showSides").get.toString(),
        gameAttr.attribute("fluctuateValuesUsingFacts").get.toString(),
        side = sideList,
        category = categoryList,
        player = playerList,
        statisticsPeriod = statisticList
      )

      listOfGame = listOfGame :+ listOfGame1
    }

    listOfGame

  }
}
