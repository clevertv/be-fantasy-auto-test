package com.espn.test.model.domain

case class GetPlayersFromWeb(
                              categoryId: Int,
                              code: String
                            )