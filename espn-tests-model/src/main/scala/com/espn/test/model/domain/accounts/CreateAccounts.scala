package com.espn.test.model.domain.accounts

abstract class Accounts

case class CreateAccounts(
                           profile: Profile,
                           legalAssertions: Seq[String],
                           marketing: Option[Seq[Marketing]] = None,
                           password: String,
                           userId: Option[Int] = None
                         )

case class Profile (
                     phones: Seq[AnyRef],
                     region: String,
                     gender: String,
                     addresses: Seq[AnyRef],
                     firstName: String,
                     lastName: String,
                     email: String
                   )

case class Marketing (
                       code: String,
                       subscribed: Boolean,
                       textID: String
                     )