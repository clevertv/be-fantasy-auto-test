package com.espn.test.model.domain.uploads

import java.util
import java.util.ArrayList

//class League[String]{
//}

case class Competition (
                        clientName: String,
                        gameName: String,
                        competitionName: String,
                        registrationStartDate: String,
                        registrationEndDate: String,
                        maximumEntriesPerUser: String,
                        entryCost: String,
                        cupCost: String,
                        premiumSurcharge: String,
                        currency: String,
                        freeQualification: String,
                        freeGames: String,
                        freeDiscount: String,
                        globalLeagueName: String,
                        statisticsPeriodLeague: String,
                        randomLeagueName: String,
                        duplicateTeamNames: String,
                        AffinityLeague: AffinityLeague,
                        FantasyCompetition: FantasyCompetition
                      ) extends SelectCom

case class AffinityLeague (
                           league: util.ArrayList[String]
                         )

case class FantasyCompetition(
                               minimumPlayersPerSide: String,
                               maximumPlayersPerSide: String,
                               maximumTransfers: String,
                               minimumTransferCost: String,
                               unlimitedTransfersCost: String,
                               playersInSquad: String,
                               fluctuatingPlayerValues: String,
                               captainCount: String,
                               maximumCaptainChanges: String,
                               unlimitedSMSCost: String,
                               autoSubs: String,
                               Category: ArrayList[CompetitionCategory],
                               TransferPeriod: ArrayList[TransferPeriod],
                               PartialPrize: ArrayList[PartialPrize],
                               OverallPrize: OverallPrize,
                               ActivationDate: ArrayList[String]
                             )

case class CompetitionCategory(
                                name: String,
                                minimumInSquad: String,
                                maximumInSquad: String,
                                minimumInTeam: String,
                                maximumInTeam: String,
                                defaultInTeam: String,
                                minimumCaptainCount: String,
                                maximumCaptainCount: String
                              )

case class TransferPeriod(
                           maxTransfers: String,
                           includeInOverallLimit: String,
                           startDate: String,
                           endDate: String,
                           transferCost: String,
                           maximumCaptainChanges: String
                         )

case class PartialPrize(
                         name: String,
                         startDate: String,
                         endDate: String,
                         subType: String
                       )

case class OverallPrize(
                         name: String
                       )

trait aaa {
  type League = String
}

