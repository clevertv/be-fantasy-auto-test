package com.espn.test.model.utils

import io.restassured.builder.RequestSpecBuilder
import io.restassured.specification.RequestSpecification


object Specifications {

  def registrations: RequestSpecification =
    buildRequestSpetification("https://registerdisney.go.com", 999)

  private def buildRequestSpetification(
                                       url: String,
                                       port: Int,
                                       contentType: String = "application/json; charset=utf-8",
                                       mediaTypes: String = "application/json, text/plain"
                                       ) : RequestSpecification = {
    new RequestSpecBuilder()
      .setBaseUri(url)
      .setPort(port)
      .setContentType(contentType)
      .setAccept(mediaTypes)
      .addHeader("X-Client-Req-Id", Randomizer.uuid())
      .build()
  }

}
