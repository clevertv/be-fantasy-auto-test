package com.espn.test.model.utils

import java.nio.charset.StandardCharsets
import java.nio.file.{Files, Paths}

class SaveFile {

  def saveToFile(fileName: String, data: String ): Unit = {
    Files.write(Paths.get(s"""./espn-tests-perf/src/main/resources/feed/$fileName.xml"""), data.getBytes(StandardCharsets.UTF_8))
  }

}
