package com.espn.test.model.samplers

import com.espn.test.model.domain.accounts.{CreateAccounts, Profile}
import com.espn.test.model.utils.Randomizer._

object CrearteAccount {

  val rAlfaNum = scala.util.Random

  def createCustomer(): CreateAccounts = new CreateAccounts(
      profile = profile(),
    legalAssertions = Seq ("GTOU", "cookie_ppv2_proxy"),
    password = "1234567890q"
  )

  def profile() = new Profile(
    phones = Seq.empty,
    region = "MD",
    gender = "0",
    firstName = getAlpha(12),
    addresses = Seq.empty,
    lastName = firstName,
    email = getAlpha(10) + "@" + getAlpha(10) + ".com"
  )
}
