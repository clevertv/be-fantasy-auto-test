package com.espn.test.model.domain.accounts

case class ReceivedUserAccount (
                                 result_message: String,
                                 result_status: String,
                                 externalId: String,
                                 result_code: String,
                                 ctv_user_id: Int
                               )
