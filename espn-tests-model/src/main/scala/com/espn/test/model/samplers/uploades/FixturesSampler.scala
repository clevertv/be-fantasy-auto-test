package com.espn.test.model.samplers.uploades

import com.espn.test.model.domain.uploads.Game
import com.espn.test.model.utils.{Randomizer, SaveFile}

object FixturesSampler extends SaveFile {

  def createFixtures(gameData: Game, gameIdFromDB: String): String = {

    val prettyPrinter = new scala.xml.PrettyPrinter(10000, 4)

    var events = Array[Map[String, String]]()

    val teams = gameData.side

    for (i <- 0 until teams.length / 2) {
      for (j <- teams.length / 2 until teams.length)
        if (events.exists(h => h.mkString.contains(teams(i).sideName) || h.mkString.contains(teams(j).sideName))) {
          Nil
        } else {
          events = events :+ Map(teams(i).sideName -> teams(j).sideName)
        }
    }

    val fixtures = <GameTips gameId={gameIdFromDB}>
      {events.map(e => {
        <Fixture date={Randomizer.dateTime.ISO8601.now()}
                 description={e.keys.mkString + " vs " + e.values.mkString}
                 home={e.keys.mkString}
                 away={e.values.mkString}></Fixture>
      })}

    </GameTips>


    val fixtureXML = prettyPrinter.format(fixtures)

    saveToFile(s"Fixture", fixtureXML)
    fixtureXML
  }

}
