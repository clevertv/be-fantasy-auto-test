import sbt.Keys._

//enablePlugins(JniNative)

libraryDependencies ++= Seq(
  "com.thoughtworks.xstream" % "xstream" % "1.4.11.1",
  "org.joda" % "joda-convert" % "1.2",
  "joda-time" % "joda-time" % "2.9.9",
  "org.scalatest" %% "scalatest" % "3.2.0-SNAP10",
  "com.typesafe" % "config" % "1.3.4",
  "com.jayway.jsonpath" % "json-path" % "2.4.0",
  "com.fasterxml.jackson.module" %% "jackson-module-scala" % "2.9.8",
  "org.json4s" %% "json4s-scalap" % "3.6.7",
  "org.json4s" %% "json4s-jackson" % "3.6.7",
  "io.rest-assured" % "scala-support" % "4.0.0",
  "com.typesafe.scala-logging" %% "scala-logging" % "3.9.2",

  "com.jayway.restassured" % "json-schema-validator" % "2.8.0",
  "com.jayway.jsonpath" % "json-path" % "2.1.0",
  "com.jayway.restassured" % "scala-support" % "2.8.0",

  "io.circe" %% "circe-core" % "0.11.1",
  "io.circe" %% "circe-generic" % "0.11.1",
  "io.circe" %% "circe-parser" % "0.11.1",

  "net.liftweb" %% "lift-json" % "3.3.0",

  "io.spray" %%  "spray-json" % "1.3.4"

)

