

name := "be-fantasy-auto-test"

lazy val commonSettings = Seq(
  organization := "codefactory.com",
  version := "0.1.0",
  scalaVersion := "2.12.8",
  scalacOptions := Seq("-unchecked", "-deprecation", "-feature", "-encoding", "utf8", "-language:reflectiveCalls", "-language:postfixOps"),
  javaOptions += "-Xmx2G"
)

lazy val `be-fantasy-auto-test` = (project in file("."))
  .aggregate(`espn-tests-model`, `espn-tests-perf`) //.dependsOn(`espn-tests-model`)

lazy val `espn-tests-model` = project
  .settings(
    commonSettings: _*
  )

lazy val `espn-tests-perf` = project
  .settings(commonSettings: _*)
  .dependsOn(`espn-tests-model`)


commonSettings